import subprocess

input_strings = ["", "test", "first word", "cool word", "Наше дело не так однозначно, как может показаться: повышение уровня гражданского сознания создаёт необходимость включения в производственный план целого ряда внеочередных мероприятий с учётом комплекса дальнейших направлений развития. Значимость этих проблем настолько очевидна, "]
expected_output = ["", "third word explanation", "first word explanation", "", ""]
expected_error = ["word is not found", "",  "", "word is not found", "buffer overflow exception"]

for i in range(len(input_strings)):
    process = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=input_strings[i].encode())
    if process.returncode == -11:
        print("Segmentation fault")   
    else:
        actual_output = stdout.decode().strip()
        actual_error = stderr.decode().strip()
        test_number = i + 1 

        if actual_output == expected_output[i] and actual_error == expected_error[i]:
            print(f"Test {test_number} passed")
        else:
            print(f"Test {test_number} failed.")
            print(f"Output: {actual_output}, Expected Output: {expected_output[i]}")
            print(f"Error: {actual_error}, Expected Error: {expected_error[i]}")
