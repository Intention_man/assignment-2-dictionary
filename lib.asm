global exit
global string_length
global print_string
global print_error
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_sentence
global parse_uint
global parse_int
global string_copy


section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60; код для системного вызова
    xor rdi, rdi
    syscall
    

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax 
    .loop:
        cmp byte [rdi+rax], 0
        je .end 
        inc rax 
        jmp .loop
    .end:
        ret
        

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi ; save string adress on stack 
    call  string_length   ; call function to get string length
    pop rdi
    mov rsi, rdi 
    mov rdx, rax
    mov rax, 1           ; 'write' syscall number
    mov rdi, 1           ; stdout descriptor
    syscall
    ret

    ; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi ; save string adress on stack 
    call  string_length   ; call function to get string length
    pop rdi
    mov rsi, rdi 
    mov rdx, rax
    mov rax, 1           ; 'write' syscall number
    mov rdi, 2           ; stdout descriptor
    syscall
    ret
    

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, 1        
    mov rdi, 1 
    mov rsi, rsp
    mov rdx, 1          
    syscall    
    pop rdi       
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 10       ; Делитель (10)
    mov rax, rdi      ; rax будет использоваться для хранения адреса строки
    push 0 ; это "нуль-терминал" для числа в стеке
    
    .loop:
        xor rdx, rdx      ; Сбрасываем остаток от предыдущего деления
        div rcx           ; Делим rax на 10, результат в rax, остаток в rdx
        add dl, '0'       ; Преобразуем остаток в ASCII символ
        push rdx
        
        test rax, rax
        jnz .loop         ; Если нет, продолжаем цикл

    .print_chars_from_stack:
        pop rax
        cmp al, 0
        je .end

        mov rdi, rax
        call print_char
        
        jmp .print_chars_from_stack

    .end:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print_unsigned_part ; Проверка на неотрицательность
    push rdi
    mov rdi, '-'      ; Вывод знака "-"
    call print_char

    pop rdi
    neg rdi
    .print_unsigned_part:
        call print_uint   ; Вывод положительного числа

    ret     

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop:
        mov cl, byte [rdi+rax]
        mov dl, byte [rsi+rax]

        cmp cl, dl
        jne .is_not_equal

        cmp cl, 0
        je .is_equal

        inc rax
        jmp .loop

    .is_equal:
        mov rax, 1
        ret
    
    .is_not_equal:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0 ; выделяем место для 1 символа       
    mov rdi, 0 
    mov rsi, rsp ; для считывания на стек в выделенное место
    mov rdx, 1          
    syscall    
    pop rax  
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi
    xor rcx, rcx
    xor rax, rax

    .skip_spaces:
        push rdi
        push rsi
        call read_char  ; читаем символ слова
        pop rsi
        pop rdi

        cmp rax, 0x20
        je .skip_spaces
        cmp rax, 0x9
        je .skip_spaces
        cmp rax, 0xA
        je .skip_spaces
        xor rcx, rcx
    .read_by_symbol:
        cmp rax, 0 ; если 0, выходим из цикла
        je .ok
        cmp rax, 0x20 ; если символ пробельный, выходим из цикла
        je .ok
        cmp rax, 0x9
        je .ok
        cmp rax, 0xA
        je .ok
        mov byte [rdi], al
        
        cmp rcx, rsi ; проверка, не превышен ли размер буфера
        jg .exception

        inc rcx ; увеличиваем счетчик длины слова
        inc rdi ; устанавливаем адрес нового символа
        
        push rdi
        push rsi
        push rcx
        call read_char  ; читаем сивол слова
        pop rcx
        pop rsi
        pop rdi

        jmp .read_by_symbol

    .exception:
        mov rax, 0
        pop rdi
        jmp .end

    .ok:  
        mov rdx, rcx
        pop rax
        
    .end:
        ret

; Как read_word, но пробел и таб не являются символами завершения 
read_sentence: 
    push rdi
    xor rcx, rcx
    xor rax, rax

    .skip_spaces:
        push rdi
        push rsi
        call read_char  ; читаем символ слова
        pop rsi
        pop rdi

        cmp rax, 0x20
        je .skip_spaces
        cmp rax, 0x9
        je .skip_spaces
        cmp rax, 0xA
        je .skip_spaces
        xor rcx, rcx
    .read_by_symbol:
        cmp rax, 0 ; если 0, выходим из цикла
        je .ok
        cmp rax, 0xA
        je .ok
        mov byte [rdi], al
        
        cmp rcx, rsi ; проверка, не превышен ли размер буфера
        jg .exception

        inc rcx ; увеличиваем счетчик длины слова
        inc rdi ; устанавливаем адрес нового символа
        
        push rdi
        push rsi
        push rcx
        call read_char  ; читаем сивол слова
        pop rcx
        pop rsi
        pop rdi

        jmp .read_by_symbol

    .exception:
        mov rax, 0
        pop rdi
        jmp .end

    .ok:  
        mov rdx, rcx
        pop rax
        
    .end:
        ret

parse_uint:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
    .parse_by_symbol:
        ; cmp rdx, 63
        ; jo .overflow 
        mov cl, byte [rdi] ; чтобы формировать число посимвольно
        sub rcx, 0x30   ; получаем из ASCII кода числа предполагаемую цифру
        cmp rcx, 0      ; проверяем, что это действительно цифра
        jl .end
        cmp rcx, 9
        jg .end

        push rdx
        mul r8    ; умножаем полученное на прошлом шаге число на 10
        jo .overflow 
        pop rdx
        add rax, rcx ; прибавляем новую младшую цифру
        jo .overflow 
        
        inc rdx ; увеличиваем счетчик длины слова
        inc rdi ; устанавливаем адрес нового символа
        jmp .parse_by_symbol
    
    .overflow:
        mov rdx, 0

    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rcx, rcx
    xor rdx, rdx
    xor rax, rax
    mov r8, 10
       
    mov cl, byte [rdi] ; чтобы формировать число посимвольно

    cmp rcx, "-" ; проверяем, минус ли первый символ
    jz .is_minus

    push rdi
    call parse_uint
    pop rdi
    jmp .end 

    .is_minus:
        inc rdi
        push rdi
        call parse_uint
        pop rdi
        cmp rdx, 0
        jz .end
        neg rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rcx, rdx
    xor rdx, rdx
    xor rax, rax
    dec rsi

    .copy_by_symbol:
        mov r8b, byte [rdi]
        cmp r8b, 0
        jz .ok
        inc rdi
        
        cmp rdx, rcx
        jge .overflow
        inc rdx
        inc rsi
        mov byte [rsi], r8b
        
        jmp .copy_by_symbol

    .overflow:
        xor rax, rax
        jmp .end
    
    .ok:
        mov rax, rdx
        mov byte [rsi + 1], 0

    .end:
        ret