%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUF_SIZE 256

section .bss
buffer_string: resb BUF_SIZE

section .rodata
test_string db "key", 0
buffer_overflow_mes db "buffer overflow exception", 0
not_found_mes db "word is not found", 0

section .text
global _start


;- Читает строку размером не более 255 символов в буфер с `stdin`.
;- Пытается найти вхождение в словаре; если оно найдено, распечатывает в `stdout` значение по этому ключу. Иначе выдает сообщение об ошибке.
_start:
    mov rdi, buffer_string ; устанавливаем аргументы для чтения слова
    mov rsi, BUF_SIZE
    ; Принимает: адрес начала буфера, размер буфера
    call read_sentence
    test rax, rax ; если rax==0 -> слово не влезло в буфер -> ошибка
    jz .buffer_overflow

    ; mov rdi, buffer_string
    ; mov rsi, test_string
    ; call string_equals
    ; mov rdi, rax
    ; call print_uint

    ; mov rdi, dict_cursor
    ; ; sub rdi, 8
    ; add rdi, 8
    ; call print_string
    ; mov rdi, test_string    

    mov rdi, buffer_string  ; устанавливаем аргументы для find_word
    mov rsi, dict_cursor
    call find_word ;  в результате в rax лежит адрес начала ключа слова в словаре, если слово найдено
    test rax, rax
    jz .not_found
    jmp .success

    .buffer_overflow:
        mov rdi, buffer_overflow_mes
        call print_error
        jmp .end
        
    .not_found:
        mov rdi, not_found_mes
        call print_error
        jmp .end

    .success:
        mov r8, rax ; сохраняем адрес ключа
        mov rdi, rax 
        call string_length ; определяем длину ключа
        add r8, rax 
        inc r8 ; в r8 теперь лежит адрес значения
        mov rdi, r8
        call print_string

    .end:
        xor rdi, rdi
        call exit