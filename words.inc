%include "colon.inc"

section .data

colon "test", third_word
db "third word explanation", 0

colon "second word", second_word
db "second word explanation", 0 

colon "first word", first_word
db "first word explanation", 0 

colon "Наше дело не так однозначно, как может показаться: повышение уровня гражданского сознания создаёт необходимость включения в производственный план целого ряда внеочередных мероприятий с учётом комплекса дальнейших направлений развития. Значимость этих проблем настолько очевидна, ", very_long_word
db "This is a long value", 0 