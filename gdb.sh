#!/bin/bash

# запускать: ./gdb.sh *название файла без расширения*
# например:  ./gdb.sh test

# # Проверка наличия аргумента (названия файла)
# if [ $# -ne 1 ]; then
#     echo "Использование: $0 <название_файла>"
#     exit 1
# fi

# # Название файла, переданное как аргумент
# file_name="$1"


# Запускаем GDB с командами
gdb -q -ex "file main" -ex "break _start" -ex "layout asm" -ex "layout reg" -ex "run"


# Завершение скрипта
exit 0