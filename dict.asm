%include "lib.inc"
global find_word

section .data
cell_size db 8

section .text

; Если подходящее вхождение найдено, вернёт адрес *начала вхождения в словарь* (не значения), иначе вернёт 0. 
; rdi - key pointer
; rsi - dict cursor
find_word:
    xor rax, rax
    .loop:
        mov rax, rsi  ; if current adress
        test rax, rax   ; stores null value 
        jz .not_found   ; -> dict hasn't this value

        add rsi, [cell_size] ; the rsi stores stores key adress
        push rsi
        call string_equals ; compare
        pop rsi
        test rax, rax   ; rax == 0 
        jnz .success    ; -> the desired key is found

        sub rsi, [cell_size] ;  the rsi stores a reference to the label of the previous element
        mov rsi, [rsi] ; go to the next element
        jmp .loop   ; jump to next cell

    .not_found:
        xor rax, rax    ; ret 0
        ret

    .success:
        mov rax, rsi ; mov found key address to the rax
        ret