%define dict_cursor 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
                dq dict_cursor
                db %1, 0
                %define dict_cursor %2
        %else
			%error "2 arg isn't a label"
		%endif
	%else
		%error "1 arg isn't a string"
	%endif
%endmacro