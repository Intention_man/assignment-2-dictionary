AS=nasm
ASFLAGS=-f elf64  # Здесь можно указать флаги для nasm, если нужно

SRCS=$(wildcard *.asm)
OBJS=$(SRCS:.asm=.o)

.PHONY: clean test all


all: $(OBJS)
	ld -o main $(OBJS)

dict.o: dict.asm lib.inc
main.o: dict.inc lib.inc words.inc

%.o: %.asm
	$(AS) $(ASFLAGS) -o $@ $<
	
test: all
	python3 ./test.py

debug:
	# make
	gdb debug -q -ex "layout asm" -ex "layout regs"

clean:
	rm -f $(OBJS) ./main